# CanvasOS
An open-source web OS based off of Canvas' UI.

## Why?
Because why not? This web OS uses Canvas' navigation bar as the taskbar, but the Window System is made entirely by me.

## License
One CSS file and a couple fonts for the icons were taken from Canvas.
